import AWS from 'aws-sdk';

export const BUCKET_REGION = 'us-east-1';

export const S3 = new AWS.S3({ region: BUCKET_REGION });

export const BUCKET_NAME = 'quicksales';
export const BUCKET_END_POINT = 's3';

export const getObjectMetadata = key =>
  new Promise((resolve, reject) =>
    S3.headObject(
      { Bucket: BUCKET_NAME, Key: key },
      (err, data) => (err ? reject(err) : resolve(data))
    )
  );

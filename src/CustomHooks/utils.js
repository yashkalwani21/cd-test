export const HOOKS_ERROR_MESSAGES = {
  ARGS_NOT_FOUND: 'Custom hook called without any args',
  ARGS_NOT_OBJECT: 'Argument passed to custom hook is not an object'
};

export const validateHooksArgs = requiredData => {
  if (!requiredData) {
    console.warn(HOOKS_ERROR_MESSAGES.ARGS_NOT_FOUND);
    return false;
  }

  if (typeof requiredData !== 'object') {
    console.warn(HOOKS_ERROR_MESSAGES.ARGS_NOT_OBJECT);
    return false;
  }

  return true;
};

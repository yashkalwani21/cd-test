import { connector } from './ApiAndCacheConnector';
import CacheRequest from './CacheRequest';
import Api from 'qs-services/Api';
import CategoryMapper from '../Helpers/CategoryMapper';
import CurrencyMapper from '../Helpers/CurrencyMapper';
import { PLAN_TYPES, saveCurrentPlan } from './FeatureUsage';
import { userUnauthorized } from './Authentication';
import { getUserEmail } from './User';

// TODO: move this to more common location with a generic name like
const COMPANY_LOCAL_STORAGE_KEYS = {
  COMPANY_META: 'COMPANY_META',
  USER_META: 'USER_META',
  AUTH_UUID: 'AUTH_UUID'
};

// Company settings
const attachCompanySettingsListener = listener => {
  const key = `${connector.COMPANY_SETTINGS.cacheKey}`;
  CacheRequest.attachListener(key, listener);
};

const removeCompanySettingsListener = listener => {
  const key = `${connector.COMPANY_SETTINGS.cacheKey}`;
  CacheRequest.removeListener(key, listener);
};

const getCompanySettings = () => {
  const key = `${connector.COMPANY_SETTINGS.cacheKey}`;
  const apiName = connector.COMPANY_SETTINGS.apiFunction;
  return CacheRequest.makeRequest(key, apiName, {
    params: [],
    options: {
      shouldNotStoreInNative: true
    }
  });
};

// Company meta
const attachCompanyMetaListener = listener => {
  const key = connector.COMPANY_META.cacheKey;
  CacheRequest.attachListener(key, listener);
};

const removeCompanyMetaListener = listener => {
  const key = connector.COMPANY_META.cacheKey;
  CacheRequest.removeListener(key, listener);
};

/*
 * @description Sets the company object in RAM and localstorage and notifies all listeners of this change
 * @params {Object} data - Entire company object
 * @params {Object} data.address - address of the company
 * @params {Object} data.catalogues - all catalogue ids of the company
 * @params {Object} data.categories - predefined category the company belongs to
 * @params {string} data.companyRepUserId - user id of the company
 * @params {string} data.currencyCode - currency code of that company
 * @params {string} data.currencyCode - currency code of that company
 * @params {string} data.logoExists - Company has logo or not
 * @params {string} data.logoVersion - Version of the logo
 * @params {string} data.name - Name of the company
 * @params {Object} data.products - All existing productIds of the company
 * @params {Object} data.showcase - All existing showcaseIds of the company
 */

const setCompanyMetaInCache = data => {
  const key = connector.COMPANY_META.cacheKey;
  CacheRequest.setCacheForKey(key, data);
  const localStorageData = JSON.stringify(data);
  localStorage.setItem(COMPANY_LOCAL_STORAGE_KEYS.COMPANY_META, localStorageData);
};

const getCompanyMetaFromCache = () => {
  const key = connector.COMPANY_META.cacheKey;
  const cache = CacheRequest.getCacheForKey(key);
  return cache;
};

const rehydrateCompanyMeta = data => {
  const companyMeta = JSON.parse(data);
  const key = connector.COMPANY_META.cacheKey;
  CacheRequest.setCacheForKey(key, companyMeta);
};

const rehydrateUserMeta = data => {
  const userMeta = JSON.parse(data);
  const key = connector.USER_META.cacheKey;
  CacheRequest.setCacheForKey(key, userMeta);
};

const getCompanySettingsFromCache = () => {
  const key = `${connector.COMPANY_SETTINGS.cacheKey}`;
  const cache = CacheRequest.getCacheForKey(key);

  return cache
    ? {
        stockVisibility: cache.showOutOfStockProduct,
        orderOnOOS: cache.allowOrdersOnOutOfStock,
        trackInventory: cache.trackInventory,
        autoReduce: cache.autoReduceInventory,
        loading: false,
        error: null,
        refreshing: false
      }
    : {
        stockVisibility: false,
        orderOnOOS: false,
        trackInventory: false,
        autoReduce: false,
        loading: true,
        error: null,
        refreshing: false
      };
};

// Helper functions
const setCompanySettingsInCache = ({ data }) => {
  const key = connector.COMPANY_SETTINGS.cacheKey;
  CacheRequest.setCacheForKey(key, data);
};

const changeCompanyOutOfStockVisibility = ({ value }) => {
  return Api.changeCompanyOutOfStockVisibility({
    value
  });
};

const changeCompanyAllowOrderOnOutOfStock = ({ value }) => {
  return Api.changeCompanyAllowOrderOnOutOfStock({
    value
  });
};

const changeCompanyTrackInventory = ({ value }) => {
  return Api.changeCompanyTrackInventory({
    value
  });
};

const changeCompanyAutoReduceInventory = ({ value }) => {
  return Api.changeCompanyAutoReduceInventory({
    value
  });
};

const getCompanyCurrencyCode = () => {
  const key = connector.COMPANY_META.cacheKey;
  const cache = CacheRequest.getCacheForKey(key);
  if (!cache) {
    return null;
  }
  return cache.currencyCode;
};

const getCompanyMetaForUserDetails = () => {
  const key = connector.COMPANY_META.cacheKey;
  const cache = CacheRequest.getCacheForKey(key);
  if (!cache) {
    return {
      name: '',
      currency: '',
      category: ''
    };
  }
  const categoryCode = Object.keys(cache.categories || {})[0];
  const category = CategoryMapper[categoryCode];

  return cache
    ? {
        name: cache.name,
        currency: cache.currencyCode,
        category
      }
    : {};
};

const getCompanyIdFromCache = () => {
  const key = connector.COMPANY_META.cacheKey;
  const cache = CacheRequest.getCacheForKey(key);

  if (!cache) {
    const parsedCompanyObject = JSON.parse(
      localStorage.getItem(COMPANY_LOCAL_STORAGE_KEYS.COMPANY_META)
    );
    return parsedCompanyObject && parsedCompanyObject.id ? parsedCompanyObject.id : null;
  }

  return cache.id || null;
};

const getCompanyCurrencySymbol = () => {
  const key = connector.COMPANY_META.cacheKey;
  const cache = CacheRequest.getCacheForKey(key);
  let currencyCode = null;
  if (cache && cache.currencyCode) {
    currencyCode = cache.currencyCode;
  }

  if (!currencyCode) {
    return '';
  }

  return CurrencyMapper[currencyCode] || '';
};

const getCompanyMetaForUserBar = () => {
  const key = connector.COMPANY_META.cacheKey;
  const cache = CacheRequest.getCacheForKey(key);

  if (!cache) {
    return {
      name: ''
    };
  }

  return {
    name: cache.name
  };
};

const getAllCatalogueCount = () => {
  const key = connector.COMPANY_META.cacheKey;
  const cache = CacheRequest.getCacheForKey(key);

  return cache && cache.catalogues ? Object.keys(cache.catalogues || {}).length : null;
};

const getAllProductCount = () => {
  const key = connector.COMPANY_META.cacheKey;
  const cache = CacheRequest.getCacheForKey(key);

  return cache && cache.products ? Object.keys(cache.products || {}).length : null;
};

const getCompanyDomain = () => {
  const key = connector.COMPANY_META.products;
  const cache = CacheRequest.getCacheForKey(key);

  if (!cache) {
    return null;
  }

  return cache && cache.companyDomain ? cache.companyDomain : null;
};

const getCompanyStockSetting = async () => {
  const key = `${connector.COMPANY_SETTINGS.cacheKey}`;

  const data = await Api.getCompanySettings();
  CacheRequest.setCacheForKey(key, data);

  return data.settings.showOutOfStockProduct;
};

const getCompanyOrderOnOOS = async () => {
  const key = `${connector.COMPANY_SETTINGS.cacheKey}`;

  const data = await Api.getCompanySettings();
  CacheRequest.setCacheForKey(key, data);

  return data.settings.allowOrdersOnOutOfStock;
};

const getCompanyCategory = () => {
  const key = connector.COMPANY_META.cacheKey;
  const cache = CacheRequest.getCacheForKey(key);
  return cache.categories || {};
};

const validateCompanyPaymentPlan = async () => {
  const currentPlan = await Api.getCurrentPlan();
  saveCurrentPlan(currentPlan);

  if (currentPlan.planType === PLAN_TYPES.LITE) {
    const email = getUserEmail();
    userUnauthorized({ email, reason: 'User is on LITE plan' });
    return;
  }
};

export {
  COMPANY_LOCAL_STORAGE_KEYS,
  attachCompanySettingsListener,
  removeCompanySettingsListener,
  getCompanySettings,
  attachCompanyMetaListener,
  removeCompanyMetaListener,
  setCompanyMetaInCache,
  setCompanySettingsInCache,
  getCompanySettingsFromCache,
  changeCompanyOutOfStockVisibility,
  changeCompanyAllowOrderOnOutOfStock,
  changeCompanyTrackInventory,
  changeCompanyAutoReduceInventory,
  getCompanyCurrencyCode,
  getCompanyMetaForUserDetails,
  getCompanyIdFromCache,
  getCompanyCurrencySymbol,
  getCompanyMetaForUserBar,
  getAllCatalogueCount,
  getAllProductCount,
  getCompanyDomain,
  rehydrateCompanyMeta,
  rehydrateUserMeta,
  getCompanyStockSetting,
  getCompanyOrderOnOOS,
  getCompanyMetaFromCache,
  getCompanyCategory,
  validateCompanyPaymentPlan
};

import eventbus from 'eventing-bus';

let NAVIGATION_REF = null;

const setNavigationRef = ref => {
  NAVIGATION_REF = ref;
};

const getNavigationRef = ref => NAVIGATION_REF;

const navigateToPath = path => {
  NAVIGATION_REF.history.push(path);
};

let SIDE_BARS_META = {
  EVENT_BUS: 'SIDE_BAR_META',
  showSideBar: false
};

const GRT_PM_META = {
  EVENT_BUS_KEY: 'GRT_PM_META_EVENTBUS',
  isGrtPM: null
};

const sideBarStateOnMount = () => {
  // const hideBars = window.location.href.includes('/login') || window.location.pathname === '/';
  const shouldShow =
    !window.location.href.includes('/login') && !(window.location.pathname === '/');
  SIDE_BARS_META.showSideBar = shouldShow;
  return shouldShow;
};

const showSideBars = () => {
  SIDE_BARS_META.showSideBar = true;
  eventbus.publish(SIDE_BARS_META.EVENT_BUS, true);
};
const hideSideBars = () => {
  SIDE_BARS_META.showSideBar = false;
  eventbus.publish(SIDE_BARS_META.EVENT_BUS, false);
};

const setGrtPmInCache = isGrtPm => {
  GRT_PM_META.isGrtPM = isGrtPm;
};

export {
  setNavigationRef,
  getNavigationRef,
  navigateToPath,
  SIDE_BARS_META,
  sideBarStateOnMount,
  showSideBars,
  hideSideBars,
  GRT_PM_META,
  setGrtPmInCache
};

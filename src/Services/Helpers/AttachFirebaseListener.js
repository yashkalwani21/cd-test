import { useEffect } from 'react';
import { attachDefaultListeners, removeDefaultListeners } from 'qs-services/Firebase';
import { COMPANY_LOCAL_STORAGE_KEYS, validateCompanyPaymentPlan } from 'qs-data-manager/Company';
import { rehydrationService } from 'qs-services/Rehydration';
import Mixpanel from 'qs-data-manager/Mixpanel';
import { setSentryContext } from './ErrorReporting';

export default ({ children, loginState }) => {
  useEffect(() => {
    if (loginState.isLoggedIn && !loginState.loggingIn) {
      rehydrationService();
      validateCompanyPaymentPlan();

      // TODO: make a common localStorage function that will always parse and get data from local storage
      const companyId = (
        JSON.parse(localStorage.getItem(COMPANY_LOCAL_STORAGE_KEYS.COMPANY_META)) || {}
      ).id;

      const userId = (JSON.parse(localStorage.getItem(COMPANY_LOCAL_STORAGE_KEYS.USER_META)) || {})
        .id;

      const email = (JSON.parse(localStorage.getItem(COMPANY_LOCAL_STORAGE_KEYS.USER_META)) || {})
        .email;

      attachDefaultListeners({
        companyId,
        userId
      });

      Mixpanel.setSessionAttributes();
      setSentryContext({ userId, email, companyId });

      return () => {
        removeDefaultListeners({ companyId, userId });
      };
    } else {
      return () => {};
    }
  }, [loginState]);

  return children;
};

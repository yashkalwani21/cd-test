import imageCompression from 'browser-image-compression';

const options = {
  maxSizeMB: 1,
  maxWidthOrHeight: 1200,
  useWebWorker: true
};

export default files => {
  return files.map(file => {
    return imageCompression(file, options);
  });
};

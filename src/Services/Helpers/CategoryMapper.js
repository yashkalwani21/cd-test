export default {
  MFR: 'Manufacturer',
  WHS: 'Wholesaler',
  DIS: 'Distributor',
  EXP: 'Exporter',
  RET: 'Retailer',
  RES: 'Reseller',
  AGNT: 'Agent',
  BUY: 'Buyer'
};

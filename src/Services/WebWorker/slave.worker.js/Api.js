import request from 'qs-services/Request';
import Config from 'qs-config/AppConfig.js';

const getTemporaryCredentials = idToken =>
  request.get(`${Config.s3TempCredentials}?idToken=${idToken}`);

export default { getTemporaryCredentials };

import AWS from 'aws-sdk';
import { queue } from 'async-es';

import { BUCKET_REGION, BUCKET_NAME } from 'qs-config/s3';
import firebase from 'qs-config/FirebaseConfig';

import Api from './Api';

let s3Credentials = null;
let s3CredentialsPromise = null;

let s3 = null;
const UPLOAD_RECURSIVELY_MAX = 3;

const uploadCounter = {};

const uploadImage = (image, key, userId) => {
  let credentialsPromise = null;

  if (s3CredentialsPromise) {
    credentialsPromise = s3CredentialsPromise;
  } else if (
    !s3Credentials ||
    (s3Credentials.Expiration <= new Date().getTime() && !s3CredentialsPromise)
  ) {
    credentialsPromise = firebase
      .auth()
      .currentUser.getIdToken(false)
      .then(idToken => Api.getTemporaryCredentials(idToken))
      .then(credentials => {
        s3Credentials = {
          ...credentials,
          Expiration: new Date(credentials.Expiration).getTime()
        };

        AWS.config.update({
          region: BUCKET_REGION,
          credentials: new AWS.Credentials(
            credentials.AccessKeyId,
            credentials.SecretAccessKey,
            credentials.SessionToken
          ),
          correctClockSkew: true
        });

        s3 = new AWS.S3({
          region: BUCKET_REGION,
          httpOptions: {
            timeout: 0
          }
        });

        return Promise.resolve(s3Credentials);
      });
  } else {
    credentialsPromise = Promise.resolve(s3Credentials);
  }

  return credentialsPromise.then(
    credentials =>
      new Promise((resolve, reject) => {
        s3CredentialsPromise = null;

        const params = {
          Bucket: BUCKET_NAME,
          Body: image,
          Key: `${userId}/products/${key}.jpg`,
          ContentType: 'image/jpeg'
        };

        s3.upload(params, { queueSize: 1 }, (err, data) => {
          if (err) {
            reject(err);
          } else {
            resolve(data);
          }
        });
      })
  );
};

const recursiveUpload = (image, key, userId, callback) => {
  if (!uploadCounter[key]) {
    uploadCounter[key] = 0;
  }

  uploadCounter[key] += 1;

  if (uploadCounter[key] > UPLOAD_RECURSIVELY_MAX) {
    callback('ERROR');
    return;
  }

  uploadImage(image, key, userId)
    .then(data => callback(null, data))
    .catch(err => {
      s3Credentials = null;
      recursiveUpload(image, key, userId, callback);
    });
};

const q = queue(({ image, key, userId }, callback) => {
  recursiveUpload(image, key, userId, callback);
}, 1);

const uploadProductImage = (image, key, userId, productId, catalogueId) => {
  return new Promise((resolve, reject) => {
    q.push({ image, key, userId }, (err, data) => {
      if (err) {
        reject({
          err: {
            message: 'uploadProductImage: Could not upload products to s3',
            error: err
          },
          pictureId: key,
          productId,
          catalogueId
        });
      } else {
        resolve({ err: null, s3Data: data, pictureId: key, productId, catalogueId });
      }
    });
  });
};

const uploadMultipleProductImages = (allCompressedImages, userId, catalogueId) => {
  return allCompressedImages.map(({ image, imageMeta }) => {
    return uploadProductImage(image, imageMeta.pictureId, userId, imageMeta.productId, catalogueId);
  });
};

export default {
  uploadProductImage,
  uploadMultipleProductImages
};

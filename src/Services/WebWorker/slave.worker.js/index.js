import s3 from './s3';
import webworkerConfig from '../config';
import * as Sentry from '@sentry/browser';

const onError = ({ err, pictureId, productId, catalogueId, extraData }) => {
  const data = {
    err,
    s3Data: null,
    pictureId,
    productId,
    catalogueId,
    extraData
  };

  // eslint-disable-next-line no-restricted-globals
  self.postMessage({
    type: webworkerConfig.UPLOAD_IMAGES.type,
    payload: data
  });
};

export const uploadImages = async payload => {
  try {
    // images [{ productId, file, pictureId }]
    const { userId, images, catalogueId, extraData } = payload;

    const uploadPromise = s3.uploadMultipleProductImages(images, userId, catalogueId);

    uploadPromise.forEach(promise => {
      promise
        .then(data => {
          // eslint-disable-next-line no-restricted-globals
          self.postMessage({
            type: webworkerConfig.UPLOAD_IMAGES.type,
            payload: {
              ...data,
              extraData
            } // { err, s3Data, pictureId, productId, extraData }
          });
        })
        .catch(err => {
          console.error('uploadPromise: err', err);
          onError({
            err: {
              message: 'uploadProductImage: Could not upload products to s3',
              error: err.error
            },
            pictureId: err.pictureId,
            productId: err.productId,
            catalogueId: err.catalogueId,
            extraData
          });
        });
    });
  } catch (err) {
    console.error('uploadImages: Could not upload images', err);
    Sentry.captureException(err);
  }
};

import worker from 'workerize-loader!../slave.worker.js'; // eslint-disable-line import/no-webpack-loader-syntax
import eventbus from 'eventing-bus';
import workerCommonConfig from '../config';
import { onImageUploadDone } from 'qs-data-manager/Products';

let instance = worker();

eventbus.on(workerCommonConfig.eventbusKey, data => {
  switch (data.type) {
    case workerCommonConfig.UPLOAD_IMAGES.type: {
      instance.uploadImages(data.payload);
      break;
    }
    default:
      break;
  }
});

instance.addEventListener('message', ({ data = {} } = {}) => {
  switch (data.type) {
    case workerCommonConfig.UPLOAD_IMAGES.type: {
      onImageUploadDone({
        isPrepared: !data.payload.err,
        error: !!data.payload.err,
        pictureId: data.payload.pictureId,
        productId: data.payload.productId,
        catalogueId: data.payload.catalogueId, // this should have gone in extraData but is already being in other places
        extraData: data.payload.extraData || {}
      });

      break;
    }
    default:
      break;
  }
});

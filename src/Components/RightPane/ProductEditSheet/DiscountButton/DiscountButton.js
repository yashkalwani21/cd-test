import React from 'react';

import './DiscountButton.css';

const DiscountButton = ({ buttonActive, onClick }) => {

  const discountButtonStyle = buttonActive ? ' discountButtonEnabled' : ' discountButtonDisabled'

  return (
    <div className='buttonContainer'>
      <button disabled={!buttonActive} onClick={ buttonActive ? onClick : () => {}} className={'discountButton' + discountButtonStyle}>
        Offer discount
      </button>
    </div>
  )
}

export default DiscountButton;

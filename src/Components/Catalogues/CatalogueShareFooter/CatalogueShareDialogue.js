import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-responsive-modal';
import WhatsappIcon from '../../../Assets/whatsapp.png';
import FacebookIcon from '../../../Assets/facebook-bg.png';
import MailIcon from '../../../Assets/mail.png';
import './CatalogueShareDialogue.css';
import Loader from '../../Loader/Loader';
import GroupShowcase from '../../../Services/DataManager/GroupShowCase';
import Showcase from '../../../Services/DataManager/Showcase';
import utils from '../../../Lib/Utils';
import { selectedCatalogueLinkSelector } from '../../../Redux/CatalogueRedux';
import Catalogue from '../../../Services/DataManager/Catalogue';

class ShareCatalogueModal extends Component {
  static propTypes = {
    catalogueIds: PropTypes.array,
    singleShowcaseLink: PropTypes.string,
    domain: PropTypes.string,
    visible: PropTypes.bool,
    onClose: PropTypes.func
  };

  state = {
    isCopied: false,
    showcaseLink: null,
    isReady: false
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.visible) {
      this.setState({
        isCopied: false,
        isReady: false,
        showcaseLink: null
      });
      this.generateShowcase(nextProps);
    }
  }

  generateShowcase = async ({ catalogueIds, singleShowcaseLink }) => {
    if (singleShowcaseLink) {
      this.setState({ showcaseLink: singleShowcaseLink, isVisible: true, isReady: true });
      return;
    } else if (catalogueIds.length === 1) {
      this.generateSingleShowcase(catalogueIds[0]);
    } else {
      this.generateMultiShowcase(catalogueIds);
    }
  };

  generateSingleShowcase = async catalogueId => {
    const { domain } = this.props;
    const title = await Catalogue.getOrAskTitle(catalogueId);

    if (!title || !title.trim()) {
      this.onCloseModal();
      return;
    }

    // const defaultShowcaseId = store.getState().catalogue.defaultShowcases[catalogueId];

    // if( defaultShowcaseId ) {
    //   console.log('from defaultShowcaseId');
    //   // link = defaultShowcaseId;
    //   const slug = 'abc';
    //   link = `${utils.getShowcaseLinkHost({ domain })}/s/${slug}`
    // } else {
    //   console.log('new created');
    //   link = await Showcase.createLink(catalogueId, { title });
    // }

    // console.log('link', link);

    const slug = await Showcase.createLink(catalogueId, { title });

    if (!slug) {
      this.onCloseModal();
    } else {
      this.setState({
        showcaseLink: `${utils.getShowcaseLinkHost({ domain })}/s/${slug}`,
        isReady: true
      });
    }
  };

  generateMultiShowcase = async catalogueIds => {
    const { domain } = this.props;
    let catIds = [];

    // why is this required? Directly pass catalogueIds. No need of catIds
    catalogueIds.forEach(c => {
      catIds.push(c);
    });

    const slug = await GroupShowcase.createLink(catIds);

    if (!slug) {
      this.onCloseModal();
    } else {
      this.setState({
        showcaseLink: `${utils.getShowcaseLinkHost({ domain })}/w/${slug}`,
        isReady: true
      });
    }
  };

  onCloseModal = () => {
    this.props.onClose();
  };

  shareCatalogue = where => {
    switch (where) {
      case 'whatsapp':
        utils.openWhatsapp({ text: this.state.showcaseLink });
        break;
      case 'email':
        utils.openMail({ text: this.state.showcaseLink });
        break;
      case 'facebook':
        utils.openFacebook({ text: this.state.showcaseLink });
        break;
      default:
        //
    }
  };

  handleTapToCopy = e => {
    e.stopPropagation();
    const { showcaseLink } = this.state;
    const el = document.createElement('textarea');
    el.value = showcaseLink;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    this.setState({ isCopied: true });
  };

  render() {
    const { isReady, showcaseLink } = this.state;

    return (
      <Modal
        open={this.props.visible}
        onClose={this.onCloseModal}
        center
        styles={{
          modal: { backgroundColor: 'white', borderRadius: 10, padding: '23px 25px 20px 25px' }
        }}
        showCloseIcon={false}
      >
        {showcaseLink && isReady ? (
          <div className="full-width">
            <div
              className="ShareCataloguePopupBody flex-c-center-center"
              onClick={this.handleTapToCopy}
            >
              <a className="CatalogueLinkText" href={showcaseLink} target="_blank" rel="noopener noreferrer">
                {showcaseLink}
              </a>
              <button className="btn-icon TapToCopyBtn">
                {this.state.isCopied ? 'Copied' : 'Click to copy'}
              </button>
            </div>
            <div className="ShareCataloguePopupFooter">
              <button
                className="flex-c-center-center"
                onClick={() => this.shareCatalogue('whatsapp')}
              >
                <img src={WhatsappIcon} alt="" />
                <span>WhatsApp</span>
              </button>
              <button
                className="flex-c-center-center"
                onClick={() => this.shareCatalogue('facebook')}
              >
                <img src={FacebookIcon} alt="" />
                <span>Facebook</span>
              </button>
              <button className="flex-c-center-center" onClick={() => this.shareCatalogue('email')}>
                <img src={MailIcon} alt="" />
                <span>Email</span>
              </button>
            </div>
          </div>
        ) : (
          <div>
            <Loader size="large" />
          </div>
        )}
      </Modal>
    );
  }
}

const mapStateToProps = (state, { catalogueIds }) => ({
  singleShowcaseLink:
    catalogueIds.length === 1 ? selectedCatalogueLinkSelector()(state, catalogueIds) : null,
  domain: state.company.domain
});

export default connect(
  mapStateToProps,
  null
)(ShareCatalogueModal);

import React, { useEffect } from 'react';
import CompanySettings from './CompanySettings';

import './styles.scss';
import WithNavRef from 'qs-helpers/WithNavRef';

export default props => {
  return (
    <WithNavRef navigationRef={props}>
      <div id={'AccountSettingsScreen'}>
        <CompanySettings />
      </div>
    </WithNavRef>
  );
};

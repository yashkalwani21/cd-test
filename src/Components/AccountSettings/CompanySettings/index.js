import React, { Component } from 'react';
import Switch from 'react-switch';
import './styles.scss';
import {
  attachCompanySettingsListener,
  removeCompanySettingsListener,
  getCompanySettings,
  getCompanySettingsFromCache,
  changeCompanyOutOfStockVisibility,
  changeCompanyAllowOrderOnOutOfStock,
  changeCompanyTrackInventory,
  changeCompanyAutoReduceInventory
} from 'qs-data-manager/Company';
import CacheListenerCallback from 'qs-helpers/CacheListenerCallback';
import { toggleGlobalLoader, getAppVersion } from 'qs-helpers';

const settingsList = ['Inventory settings'];

class CompanySettings extends Component {
  constructor(props) {
    super(props);

    const {
      stockVisibility,
      orderOnOOS,
      trackInventory,
      autoReduce,
      loading,
      error,
      refreshing
    } = getCompanySettingsFromCache();

    this.state = {
      stockVisibility,
      orderOnOOS,
      trackInventory,
      autoReduce,
      loading,
      error,
      refreshing
    };
  }

  componentDidMount() {
    attachCompanySettingsListener(this.settingsListener);
    getCompanySettings();
  }

  componentWillUnmount() {
    removeCompanySettingsListener(this.settingsListener);
  }

  settingsListener = (error, payload) => {
    const { err, loading, refreshing, data } = CacheListenerCallback(error, payload);

    if (err) {
      this.setState({
        error: err,
        loading,
        refreshing
      });
      return;
    }

    if (loading) {
      this.setState({
        error: err,
        loading,
        refreshing
      });
      return;
    }

    const {
      showOutOfStockProduct,
      allowOrdersOnOutOfStock,
      trackInventory,
      autoReduceInventory
    } = data.settings;

    this.setState({
      error: err,
      loading,
      refreshing,
      stockVisibility: showOutOfStockProduct,
      orderOnOOS: allowOrdersOnOutOfStock,
      trackInventory: trackInventory,
      autoReduce: autoReduceInventory
    });
  };

  renderSettingsList = () => {
    return settingsList.map(settingsName => (
      <div key={settingsName} className="singleRow">
        {settingsName}
      </div>
    ));
  };

  onSwitchToggle = async ({ value, toggleFrom }) => {
    const loaderKey = `onSwitchToggle${Date.now()}`;
    toggleGlobalLoader(loaderKey, true);
    const updates = {};

    let promise = null;

    if (toggleFrom === 'stockVisibility') {
      updates.stockVisibility = value;
      promise = changeCompanyOutOfStockVisibility({ value });
    } else if (toggleFrom === 'orderOnOOS') {
      updates.orderOnOOS = value;
      promise = changeCompanyAllowOrderOnOutOfStock({ value });
    } else if (toggleFrom === 'trackInventory') {
      updates.trackInventory = value;
      promise = changeCompanyTrackInventory({ value });
    } else if (toggleFrom === 'autoReduce') {
      const inventoryValue = value ? 'VISITOR' : 'COMPANY';
      updates.autoReduce = inventoryValue;
      promise = changeCompanyAutoReduceInventory({ value: inventoryValue });
    } else {
      return;
    }

    if (promise) {
      this.setState(updates);
      await promise;
    }

    toggleGlobalLoader(loaderKey, false);
  };

  render() {
    const { stockVisibility, orderOnOOS, trackInventory, autoReduce } = this.state;

    return (
      <div className="accountSettingsMainContainer">
        <div className="settingsList">
          {this.renderSettingsList()}
          <div className={'appVersion'}>
            APP VERSION: {getAppVersion()}
          </div>
        </div>

        <div className="settingsContainer">
          <div className="rowContainer">
            <div className="rowContent">
              <div className="rowHeader">Out of stock visibility</div>
              <div className="rowDescription">Show out of stock products to catalogue visitors</div>
            </div>

            <div className="rowActionButton">
              <Switch
                checked={stockVisibility}
                onChange={value => {
                  this.onSwitchToggle({ value, toggleFrom: 'stockVisibility' });
                }}
                onColor="#C0DACE"
                onHandleColor="#4DA47A"
                handleDiameter={25}
                uncheckedIcon={false}
                checkedIcon={false}
                height={17}
                width={40}
              />
            </div>
          </div>

          {!!stockVisibility && (
            <div className="rowContainer">
              <div className="rowContent">
                <div className="rowHeader">Allow orders on out of stock products</div>
                <div className="rowDescription">
                  Allow customers to place an order on out of stock products
                </div>
              </div>

              <div className="rowActionButton">
                <Switch
                  checked={orderOnOOS}
                  onChange={value => {
                    this.onSwitchToggle({ value, toggleFrom: 'orderOnOOS' });
                  }}
                  onColor="#C0DACE"
                  onHandleColor="#4DA47A"
                  handleDiameter={25}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  height={17}
                  width={40}
                />
              </div>
            </div>
          )}

          <div className="rowContainer">
            <div className="rowContent">
              <div className="rowHeader">Track inventory</div>
              <div className="rowDescription">
                Turn on inventory tracking settings by default on new products
              </div>
            </div>

            <div className="rowActionButton">
              <Switch
                checked={trackInventory}
                onChange={value => {
                  this.onSwitchToggle({ value, toggleFrom: 'trackInventory' });
                }}
                onColor="#C0DACE"
                onHandleColor="#4DA47A"
                handleDiameter={25}
                uncheckedIcon={false}
                checkedIcon={false}
                height={17}
                width={40}
              />
            </div>
          </div>

          {!!trackInventory && (
            <div className="rowContainer">
              <div className="rowContent">
                <div className="rowHeader">Auto reduce inventory on visitor confirmation</div>
                <div className="rowDescription">
                  Turn on this setting by default for new products
                </div>
              </div>

              <div className="rowActionButton">
                <Switch
                  checked={autoReduce}
                  onChange={value => {
                    this.onSwitchToggle({ value, toggleFrom: 'autoReduce' });
                  }}
                  onColor="#C0DACE"
                  onHandleColor="#4DA47A"
                  handleDiameter={25}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  height={17}
                  width={40}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default CompanySettings;

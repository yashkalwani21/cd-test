import React from 'react';
import './styles.scss';

export default ({ selectedCount = 0 }) => {
  return <div id={'BottomSheetBulkEditingHeader'}>bulk editing ({selectedCount} items)</div>;
};

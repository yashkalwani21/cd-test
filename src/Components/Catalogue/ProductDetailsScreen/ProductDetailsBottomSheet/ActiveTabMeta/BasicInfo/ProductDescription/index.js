import React, { useCallback, useState, useEffect } from 'react';

import './styles.scss';
import eventbus from 'eventing-bus';
import { SAVE_BUTTON_META, bulkSaveDescription } from 'qs-data-manager/ProductDetails';

export default ({ description, activeProductIds, activeProductId, isBulkEditing }) => {
  const [descriptionState, setDescriptionSate] = useState({
    modifiedDescription: description || '',
    showSaveButton: false
  });

  useEffect(() => {
    setDescriptionSate({
      modifiedDescription: description || '',
      showSaveButton: false
    });
  }, [isBulkEditing, description]);

  useEffect(() => {
    if (isBulkEditing) {
      setDescriptionSate(prevState => ({
        ...prevState,
        modifiedDescription: '',
        showSaveButton: false
      }));
    }
  }, [(activeProductIds || []).length, isBulkEditing]);

  const changeDescription = useCallback(
    e => {
      e.stopPropagation();
      const value = e.target.value;
      const hasChanged = (description || '') !== value;

      const showSaveButton = isBulkEditing && hasChanged;

      setDescriptionSate(prevState => ({
        ...prevState,
        modifiedDescription: value,
        showSaveButton
      }));

      if (!isBulkEditing) {
        eventbus.publish(SAVE_BUTTON_META.eventbusKey, {
          id: SAVE_BUTTON_META.PRODUCT_DESCRIPTION.id,
          hasChanged,
          data: value,
          eventType: SAVE_BUTTON_META.eventType.PRODUCT_META.id
        });
      }
    },
    [description, isBulkEditing]
  );

  const onSaveClick = useCallback(() => {
    bulkSaveDescription({
      productIds: activeProductIds,
      description: descriptionState.modifiedDescription
    }).then(() => {
      setDescriptionSate(prevState => ({
        ...prevState,
        showSaveButton: false
      }));
    });
  }, [activeProductIds, descriptionState.modifiedDescription]);

  return (
    <div id={'ProductDescription'}>
      <div className={'rowContainer'}>
        <div className={'rowHeading'}>Description</div>
        <div className={'textAreaContainer'}>
          <textarea
            rows="5"
            cols="20"
            type="text"
            onChange={changeDescription}
            value={descriptionState.modifiedDescription}
            className={`description ${descriptionState.showSaveButton ? 'restrictWidth' : ''}`}
          />
          {!!descriptionState.showSaveButton ? (
            <div className={'saveButton'} onClick={onSaveClick}>
              save
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

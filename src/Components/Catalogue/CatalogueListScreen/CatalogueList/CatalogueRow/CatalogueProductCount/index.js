import React from 'react';
import './styles.scss';

export default ({ productCount }) => {
  return <div className={'CatalogueProductCount'}>{productCount} product{ productCount > 1 && 's'}</div>;
};

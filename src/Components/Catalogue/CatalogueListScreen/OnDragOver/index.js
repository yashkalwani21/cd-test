import React from 'react';
import './styles.scss';

export default ({ isVisible }) => {
  if (!isVisible) {
    return null;
  }

  return (<div className={'dropZone'}>
    <div>
      DROP IMAGES FOLDER HERE
    </div>
  </div>);
};

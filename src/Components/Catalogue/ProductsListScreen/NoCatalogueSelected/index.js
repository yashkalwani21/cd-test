import React from 'react';

import './styles.scss'

export default () => {
  return <div id={'NoCatalogueSelected'}>
    Select a catalogue to view the list of products
  </div>
}

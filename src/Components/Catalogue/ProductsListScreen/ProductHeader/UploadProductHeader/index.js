import React, { useState, useEffect } from 'react';
import './styles.scss';
import eventbus from 'eventing-bus';
import { UPLOAD_PRODUCT_HEADER } from 'qs-data-manager/Products';
import { getImageUploadingMeta } from 'qs-data-manager/Products';

export default ({ activeCatalogueId } = {}) => {
  const [uploadMeta, setUploadMeta] = useState(() =>
    getImageUploadingMeta({ catalogueId: activeCatalogueId })
  );

  useEffect(() => {
    const meta = getImageUploadingMeta({ catalogueId: activeCatalogueId });
    setUploadMeta(meta);

    const key = UPLOAD_PRODUCT_HEADER.eventbusKey(activeCatalogueId);
    const removeListener = eventbus.on(key, ({ remaining, percent } = {}) => {
      const shouldShow = !!remaining;
      setUploadMeta({
        shouldShow,
        percent,
        remaining
      });
    });

    return () => {
      setUploadMeta({
        shouldShow: false,
        percent: 0,
        remaining: 0
      });
      removeListener();
    };
  }, [activeCatalogueId]);

  if (!uploadMeta.shouldShow) {
    return null;
  }

  return (
    <div id={'UploadProductHeader'}>
      <div className={'uploadingPictures'}>Uploading your pictures ({uploadMeta.percent}%) </div>
      <div className={'progressBarContainer'}>
        <div className={'progressBar'} style={{ width: `${uploadMeta.percent}%` }} />
      </div>
      <div className={'picturesRemaining'}>{uploadMeta.remaining} pictures remaining</div>
    </div>
  );
};

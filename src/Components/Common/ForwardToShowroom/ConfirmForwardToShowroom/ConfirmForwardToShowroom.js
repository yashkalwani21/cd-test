import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import './ConfirmForwardToShowroom.css';
import Loader from 'qs-components/Common/Loader';
import Mixpanel from 'qs-data-manager/Mixpanel';

const forwardCatalogueToShowroom = body => {
  let message = `Dear GRT Showroom,

Please click on the below link and select products to place an order - 
${body.catalogueLink}

Sent via QuickSell App
On behalf of GRT Purchase manager`;
  message = encodeURIComponent(message);
  let link = `mailto:${body.showroomEmailId[0]}?subject=New catalogue - ${
    body.catalogueName
  }&bcc=${body.showroomEmailId.join(',') + '\n'}&body=${message}`;

  const a = document.createElement('a');
  a.href = link;
  a.target = '_blank';
  a.click();
};

class ConfirmForwardToShowroom extends Component {
  static propTypes = {};

  constructor(props) {
    super(props);
    this.state = {
      showLoader: false,
      closeModal: false,
      visible: props.visible,
      isResponseSuccessful: false,
      responseReceived: false,
      responseMessage: ''
    };
  }

  componentWillMount() {}

  componentWillReceiveProps(nextProps) {
    this.setState({
      visible: nextProps.visible
    });
  }

  handleSendNow = () => {
    const { catalogueLink, catalogueName, selectedShowroomMap } = this.props;
    const selectedShowroomCount = Object.keys(selectedShowroomMap).length;
    this.setState({ showLoader: true });

    const body = {
      catalogueLink,
      catalogueName,
      showroomEmailId: Object.values(selectedShowroomMap)
    };
    forwardCatalogueToShowroom(body);
    this.props.closeForwardModal();
    Mixpanel.sendEvent({
      eventName: 'grt_send_now_clicked',
      props: {
        catalogueLink,
        selectedEmailIdsCount: selectedShowroomCount
      }
    });
  };

  handleCancel = () => {
    this.props.closeModal();
  };

  renderConfirmState = () => {
    const showroomCount = Object.keys(this.props.selectedShowroomMap).length;
    return (
      <div>
        <div className="sendToGrtShowroomTitle">Send to GRT showrooms</div>
        <div className="message">
          <div>
            Are you sure you want to send{' '}
            <span style={{ fontWeight: 'bold' }}>"{this.props.catalogueName}"</span> catalogue to{' '}
            {showroomCount} GRT {showroomCount === 1 ? 'showroom' : 'showrooms'}?
          </div>
        </div>
        <div className="confirmationButtonContainer">
          {!this.state.showLoader && (
            <div className="cancelButton" onClick={this.handleCancel}>
              Cancel
            </div>
          )}
          <div style={{ flex: 1 }} />
          {this.state.showLoader ? (
            <Loader size="small" />
          ) : (
            <div className="sendNowButton" onClick={this.handleSendNow}>
              Send now
            </div>
          )}
        </div>
      </div>
    );
  };

  renderResponseState = () => {
    const { isResponseSuccessful, responseMessage } = this.state;
    const { closeForwardModal, supportPhoneNumber } = this.props;
    return (
      <div>
        <div className={isResponseSuccessful ? 'responseSuccess' : 'responseFailed'}>
          {isResponseSuccessful ? 'Broadcast successful' : ' Broadcast failed'}
        </div>
        <div className="message">
          {isResponseSuccessful
            ? responseMessage
            : `We’ve encountered an error. Please contact ${supportPhoneNumber}`}
        </div>
        <div className="responseButtonContainer">
          <div className="okButton" onClick={closeForwardModal}>
            OK
          </div>
        </div>
      </div>
    );
  };

  render() {
    const { visible, responseReceived } = this.state;

    return (
      <Modal
        open={visible}
        center
        styles={{
          modal: {
            backgroundColor: 'white',
            borderRadius: 4,
            padding: '0'
          }
        }}
        showCloseIcon={false}
      >
        <div className="container">
          {!responseReceived ? this.renderConfirmState() : this.renderResponseState()}
        </div>
      </Modal>
    );
  }
}

export default ConfirmForwardToShowroom;

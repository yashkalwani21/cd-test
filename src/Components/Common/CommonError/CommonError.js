export default class CommonError extends Error {
  constructor({message = 'Something went wrong', ...restProperties}) {
    super()
    this.message = message
    //Assign all extra properties to the error object.
    Object.assign(this, restProperties)
  }
}

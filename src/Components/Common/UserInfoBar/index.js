import React, { useState, useEffect } from 'react';
import './styles.scss';

import UserDetails from './UserDetails';
import { ReactComponent as SettingsGear } from 'qs-assets/Media/userDetailsGear.svg';
import GlobalLoader from '../GlobalLoader';
import {
  getCompanyMetaForUserBar,
  attachCompanyMetaListener,
  removeCompanyMetaListener
} from 'qs-data-manager/Company';
import {
  getUserMetaForUserBar,
  attachUserMetaListener,
  removeUserMetaListener
} from 'qs-data-manager/User';
import CacheListenerCallback from 'qs-helpers/CacheListenerCallback';

export default () => {
  const [companyMeta, setCompanyMeta] = useState(() => {
    return getCompanyMetaForUserBar();
  });
  const [userMeta, setUserMeta] = useState(() => {
    return getUserMetaForUserBar();
  });
  const [userDetailsVisibility, setUserDetailsVisibility] = useState(false);

  // Effect to keep company data in real time
  useEffect(() => {
    const listener = (error, payload) => {
      const { err, loading, data } = CacheListenerCallback(error, payload);
      if (err || loading) {
        return;
      }

      const companyMeta = {
        name: data.name
      };

      setCompanyMeta(companyMeta);
    };

    attachCompanyMetaListener(listener);

    return () => {
      removeCompanyMetaListener(listener);
    };
  });

  // Effect to keep user data in real time
  useEffect(() => {
    const listener = (error, payload) => {
      const { err, loading, data } = CacheListenerCallback(error, payload);
      if (err || loading) {
        return;
      }

      const meta = {
        name: data.name
      };

      setUserMeta(meta);
    };

    attachUserMetaListener(listener);

    return () => {
      removeUserMetaListener(listener);
    };
  });

  const toggleUserDetails = () => {
    setUserDetailsVisibility(!userDetailsVisibility);
  };

  const closeUserDetails = () => {
    setUserDetailsVisibility(false);
  };

  return (
    <div id={'NavBarContainer'}>
      <div id={'NavBar'}>
        <div id={'ourCompanyName'}>QuickSell</div>
        <div className={'companyName'}>{companyMeta.name}</div>

        <div className={'userNameContainer'}>
          <div className={'userName'}>{userMeta.name}</div>
          <div onClick={toggleUserDetails}>
            <SettingsGear className={'settingsGear'} />
          </div>
          {!!userDetailsVisibility && <UserDetails closeUserDetails={closeUserDetails} />}
        </div>
      </div>
      <GlobalLoader />
    </div>
  );
};

import React, { useEffect, useState } from 'react';
import './styles.scss';

import { ReactComponent as Close } from 'qs-assets/Media/close.svg';
import { onLogout } from 'qs-data-manager/Authentication';
import { getUserMetaForUserDetails } from 'qs-data-manager/User';
import { getCompanyMetaForUserDetails } from 'qs-data-manager/Company';

export default ({ closeUserDetails }) => {
  const [userMeta] = useState({
    err: null,
    loading: false,
    refreshing: false,
    user: getUserMetaForUserDetails()
  });
  const [companyMeta] = useState(() => {
    return getCompanyMetaForUserDetails();
  });

  return (
    <div id={'UserDetails'}>
      <div className={'rowContainer namesContainer'}>
        <div>
          <div className={'userName'}>{userMeta.user.userName || ''}</div>

          <div className={'companyName'}>{companyMeta.name}</div>
        </div>

        <div onClick={closeUserDetails}>
          <Close className={'closeIcon'} />
        </div>
      </div>

      <div className={'rowContainer'}>
        <div className={'title'}>Phone</div>
        <div className={'titleValue'}>{userMeta.user.phone}</div>
      </div>

      <div className={'rowContainer'}>
        <div className={'title'}>User Category</div>
        <div className={'titleValue'}>{companyMeta.category}</div>
      </div>

      <div className={'rowContainer'}>
        <div className={'title'}>Currency</div>
        <div className={'titleValue'}>{companyMeta.currency}</div>
      </div>

      <div onClick={onLogout} id={'logout'}>
        Logout
      </div>
    </div>
  );
};

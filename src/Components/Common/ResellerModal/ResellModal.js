import React, { Component } from 'react';

import Modal from 'react-responsive-modal';

import api from 'qs-services/Api';
import Loader from 'qs-components/Common/Loader';

import 'toastr/build/toastr.min.css';
import './ResellModal.css';

import DecisionBox from './DecisionBox/DecisionBox';

import toastr from 'toastr';
import qs from 'query-string';
import { navigateToPath } from 'qs-data-manager';
import * as Sentry from "@sentry/browser";

class ResellModal extends Component {
  state = {
    showModal: false,
    screenStatus: 'showLoader', //showLoader, DecisionBox, showError
    noOfProducts: '',
    catalogueName: '',
    companyName: ''
  };

  constructor() {
    super();
    toastr.options = {
      positionClass: 'toast-bottom-left',
      showDuration: 300,
      hideDuration: 1000,
      timeOut: 2000
    };
  }

  async componentWillMount() {
    const parsed = qs.parse(window.location.search);

    let showCaseFetchUrl = null;
    if (parsed.showcaseSlug) {
      showCaseFetchUrl = parsed.showcaseSlug;
    } else {
      showCaseFetchUrl = localStorage.getItem('showcaseSlug');
    }

    if (!showCaseFetchUrl) {
      return;
    }

    if (showCaseFetchUrl) {
      this.setState({ showModal: true });
      this.removeSlugFromLocalStorage();
      this.fetchCatalogueId(showCaseFetchUrl);
    }
  }

  removeSlugFromLocalStorage = () => {
    localStorage.removeItem('showcaseSlug');
  };

  fetchCatalogueId = async showCaseFetchUrl => {
    try {
      const showcaseId = await api.getShowCaseIdForResell(showCaseFetchUrl);
      const catalogueId = await api.getCatalogId(showcaseId);
      if (catalogueId) {
        this.resellerCatalogueId = Object.keys(catalogueId)[0];
        this.fetchCompanyDetails(this.resellerCatalogueId);
      } else {
        console.log('err companyId not found');
        this.callError();
      }
    } catch (err) {
      console.error('fetchCatalogueId: Something went wrong', err);
      this.callError();
      Sentry.captureException(err);
    }
  };

  fetchCompanyDetails = async catalogueId => {
    try {
      const updates = {};
      const { belongsToCompanyId, productList, title } = await api.getResellersProductMeta(
        catalogueId
      );
      const urls = await this.fetchUrls(productList);
      updates.urls = urls;
      updates.noOfProducts = Object.keys(productList).length;
      updates.catalogueName = title;
      const companyName = await api.getCompanyName(belongsToCompanyId);
      updates.companyName = companyName;
      updates.screenStatus = 'DecisionBox';
      this.setState(updates);
    } catch (err) {
      console.log('err company details not found');
      this.callError();
      Sentry.captureException(err);
    }
  };

  callError = () => {
    toastr.error('Looks like something went wrong. Try again.');
    localStorage.removeItem('showcaseSlug');
    this.closeModal();
  };

  fetchUrls = async productList => {
    const allPicturesMeta = await api.getLinks(productList);
    const fivePictures = allPicturesMeta.filter((picture, index) => index < 5);
    const urls = [];
    fivePictures.forEach(picture => {
      if (picture.default_picture_id) {
        urls.push(picture.pictureUrl);
      }
    });
    return urls;
  };

  onDuplicate = async () => {
    this.setState({ screenStatus: 'showLoader' });
    const { error } = await api.duplicateCatalogue(this.resellerCatalogueId);
    if (!error) {
      toastr.success('Catalogue duplicated into your account');
      setTimeout(() => {
        this.closeModal();
      }, 1200);
      return;
    }

    this.callError();
  };

  closeModal = () => {
    navigateToPath('/catalogues');
    this.setState({ showModal: false });
  };

  render() {
    const { showModal, screenStatus, noOfProducts, catalogueName, companyName, urls } = this.state;

    return (
      <Modal
        open={showModal}
        center
        onClose={() => {
          this.setState({ showModal: true });
        }}
        styles={{ modal: { backgroundColor: 'transparent', borderRadius: 15, padding: 0 } }}
        showCloseIcon={false}
      >
        {screenStatus === 'showLoader' && (
          <div className="loaderContainer">
            <Loader size="large" />
          </div>
        )}

        {screenStatus === 'DecisionBox' && (
          <DecisionBox
            onYesClick={this.onDuplicate}
            onCancelClick={this.closeModal}
            companyName={companyName}
            noOfProducts={noOfProducts}
            catalogueName={catalogueName}
            urls={urls}
          />
        )}
      </Modal>
    );
  }
}

export default ResellModal;

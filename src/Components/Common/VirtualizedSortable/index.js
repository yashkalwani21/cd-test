// import React, { Component } from 'react';
// import { sortableContainer, sortableElement } from 'react-sortable-hoc';
// import List from 'react-virtualized/dist/commonjs/List';
//
// let listInstance = null;
//
// const SortableItem = sortableElement(({ value, style, index }) => {
//   return (
//     <li key={index} style={{ color: 'white', ...style }}>
//       {value}
//     </li>
//   );
// });
//
// class VirtualList extends Component {
//
//   rowRenderer = ({ index, key, style }) => {
//     const { items } = this.props;
//     const { value } = items[index];
//
//     return <SortableItem key={`${key}${index}`} index={index} style={style} value={value} />;
//   };
//
//   render() {
//     const { items, getRef, height, width } = this.props;
//
//     return (
//       <div style={{ position: 'relative', height, width }}>
//         <List
//           ref={getRef}
//           rowHeight={rowHeight}
//           rowRenderer={rowRenderer}
//           rowCount={rowCount}
//           width={width}
//           height={height}
//         />
//       </div>
//     );
//   }
// }
//
// export const SortableVirtualList = sortableContainer(VirtualList);

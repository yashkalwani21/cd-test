import React, { Component } from 'react'
import WhatsappIcon from '../../../Assets/whatsapp.png';
import GrtIcon from '../../../Assets/ic-grt-logo.png';
import FacebookIcon from '../../../Assets/facebook-bg.png';
import MailIcon from '../../../Assets/mail.png';
import Loader from '../../Loader/Loader';

import Utils from '../../../Lib/Utils';
import {getCataloguetitle} from '../../../Lib/Catalogue';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import store from '../../../Redux';
import CommonActions from '../../../Redux/CommonRedux';

import api from '../../.././Services/Api';

import 'toastr/build/toastr.min.css';
import './ShareCataloguePopup.css';

import toastr from 'toastr';
import downloadImage from '../../../Assets/downloadBlack.png';
import excel from '../../../Assets/excel.png';
import colouredPdf from '../../../Assets/Images/colouredPdf.png';
import brochure from '../../../Assets/Images/brochure.png';
import * as Sentry from "@sentry/browser";

const downloadImagePromise = {};
const downloadExcelPromise = {};
const downloadPdfPromise = {};
const downloadBrochurePromise = {};
const ERROR_MESSAGE = 'Something went wrong. Please try again';

class ShareCataloguePopup extends Component {

  static propTypes = {
      isGrtPM: PropTypes.bool,
      showGrtShowroomModal: PropTypes.func,
      toggleGlobalLoading: PropTypes.func,
      activeCatalogueId: PropTypes.string,
      catalogueLink: PropTypes.string,
      hideShareCataloguePop: PropTypes.func,
      toggleShareCataloguePopup: PropTypes.func,
  }

  state = {
    isCopied: false
  }

  handleTapToCopy = e => {
    e.stopPropagation();
    const { catalogueLink } = this.props;
    const el = document.createElement('textarea');
    el.value = catalogueLink;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    this.setState({isCopied: true})
  }

  componentDidMount() {
      document.querySelector('body').addEventListener('click', this.handleBodyClick)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.checkTitleExixt(nextProps.activeCatalogueId);
  }

  componentWillUnmount() {
    document.querySelector('body').removeEventListener('click', this.handleBodyClick)
  }

  checkTitleExixt = async catalogueId => {
    const title = await getCataloguetitle(catalogueId);
    if (!title || !title.trim()) {
        this.props.hideShareCataloguePop();
        return;
    }
  };

  handleBodyClick = event => {
    event.preventDefault();
    const parents = Utils.getElementParents(event.target);
    const parentsClass = parents.map(parent => parent.className);

    if (!parentsClass.includes('ShareCataloguePopup') &&
     !parentsClass.includes('ButtonContainer ShareButtonContainer')
     ) {
        if( parentsClass.includes('shareIconLabelContainer') ) {
            return;
        }
        this.props.toggleShareCataloguePopup(false);
    }
  }

  openGrtModal = () => {
    const { catalogueLink, showGrtShowroomModal, activeCatalogueId } = this.props;
    const catalogueName = store.getState().catalogue.catalogues[activeCatalogueId].title;
    if(catalogueLink) {
        showGrtShowroomModal(catalogueLink, catalogueName);
    }
  }

  downloadFileFromLink = link => {
    const a = document.createElement('a');
    a.href = link;
    a.click();
  }

  downloadCatalogueImages = async () => {
    const { activeCatalogueId: catalogueId, toggleGlobalLoading } = this.props;

    if(downloadImagePromise[catalogueId]) {
        return;
    }

    toggleGlobalLoading(true);

    const promise = new Promise( async (resolve, reject) => {
        try {
            const response = await api.downloadCatalogueImages(catalogueId);
            resolve(response);
        } catch(err) {
            reject(err);
            delete downloadImagePromise[catalogueId];
            toggleGlobalLoading(false);
            toastr.error(ERROR_MESSAGE);
            console.log('err in downloading images', err);
          Sentry.captureException(err);
        }
    });

    downloadImagePromise[catalogueId] = promise;

    const { zipLink } = await promise;

    delete downloadImagePromise[catalogueId];
    this.downloadFileFromLink(zipLink);
    toggleGlobalLoading(false);
  }

  downloadCatalogurPdf = async () => {
    const {
        activeCatalogueId: catalogueId,
        toggleGlobalLoading,
        catalogueLink
    } = this.props;

    if(downloadPdfPromise[catalogueId]) {
        return;
    }

    toggleGlobalLoading(true);

    const promise = new Promise( async (resolve, reject) => {
        try {
            const response = await api.downloadCataloguePdf({catalogueId, catalogueLink});
            resolve(response);
        } catch(err) {
            reject(err);
            delete downloadPdfPromise[catalogueId];
            toggleGlobalLoading(false);
            toastr.error(ERROR_MESSAGE);
            console.log('err in downloading images', err);
          Sentry.captureException(err);
        }
    });

    downloadPdfPromise[catalogueId] = promise;
    const { pdfLink } = await promise;

    delete downloadPdfPromise[catalogueId];
    this.downloadFileFromLink(pdfLink);
    toggleGlobalLoading(false);
  }

  downloadCatalogueBrochure = async () => {
    const {
        activeCatalogueId: catalogueId,
        toggleGlobalLoading
    } = this.props;

    if(downloadBrochurePromise[catalogueId]) {
        return;
    }

    toggleGlobalLoading(true);

    const promise = new Promise( async (resolve, reject) => {
        try {
            const response = await api.downloadCatalogueBrochure(catalogueId);
            resolve(response);
        } catch(err) {
            reject(err);
            delete downloadBrochurePromise[catalogueId];
            toggleGlobalLoading(false);
            toastr.error(ERROR_MESSAGE);
            console.log('err in downloading images', err);
          Sentry.captureException(err);
        }
    });

    downloadBrochurePromise[catalogueId] = promise;
    const { brochureLink } = await promise;

    delete downloadBrochurePromise[catalogueId];
    this.downloadFileFromLink(brochureLink);
    toggleGlobalLoading(false);
  }

  downloadCatalogueAsExcel = async() => {
    const { activeCatalogueId: catalogueId, toggleGlobalLoading } = this.props;

    if(downloadExcelPromise[catalogueId]) {
        return;
    }

    toggleGlobalLoading(true);

    const promise = new Promise( async (resolve, reject) => {
        try {
            const response = await api.downloadCatalogueAsExcel(catalogueId);
            resolve(response);
        } catch(err) {
            reject(err);
            delete downloadExcelPromise[catalogueId];
            toggleGlobalLoading(false);
            toastr.error(ERROR_MESSAGE);
            console.log('err in downloading excel', err);
          Sentry.captureException(err);
        }
    });

    downloadExcelPromise[catalogueId] = promise;

    const { excelLink } = await promise;

    delete downloadExcelPromise[catalogueId];
    this.downloadFileFromLink(excelLink);
    toggleGlobalLoading(false);
  }

  render () {
    const {
      shareCatalogue,
      generatingLink,
      catalogueLink,
      isGrtPM
    } = this.props;

    return (
      <div className="ShareCataloguePopup" onClick={e => e.stopPropagation()}>
          {
              generatingLink ? <Loader size="small" /> :
              <div className="full-width">
                  <div className="ShareCataloguePopupBody flex-c-center-center" onClick={this.handleTapToCopy}>
                      <a className="CatalogueLinkText m-b-5" href={catalogueLink} >
                          {catalogueLink}
                      </a>
                      <button className="btn-icon TapToCopyBtn">
                          {this.state.isCopied ? 'Copied' : 'Tap to copy'}
                      </button>
                  </div>
                  <div className='iconContainerForSharing'>
                    { isGrtPM &&
                        (<button className="flex-c-center-center" onClick={this.openGrtModal}>
                            <img src={GrtIcon} alt='GRT icon' />
                            <span>Showrooms</span>
                        </button>)
                    }
                    <button className="flex-c-center-center" onClick={() => shareCatalogue('whatsapp')}>
                        <img src={WhatsappIcon} alt='Whatsapp' />
                        <span>WhatsApp</span>
                    </button>
                    <button  className="flex-c-center-center"  onClick={this.downloadCatalogueBrochure}>
                        <img src={brochure} alt='brochure' />
                        <span>Brochure</span>
                    </button>
                    <button  className="flex-c-center-center"  onClick={this.downloadCatalogurPdf}>
                        <img src={colouredPdf} alt='PDF' />
                        <span>PDF</span>
                    </button>
                    <button className="flex-c-center-center" onClick={() => shareCatalogue('facebook')}>
                        <img src={FacebookIcon} alt='Facebook' />
                        <span>Facebook</span>
                    </button>
                    <button className="flex-c-center-center" onClick={() => shareCatalogue('mail')}>
                        <img src={MailIcon} alt='Email' />
                        <span>Email</span>
                    </button>
                    <button  className="flex-c-center-center"  onClick={this.downloadCatalogueImages}>
                        <img src={downloadImage} alt='Download catalogue' />
                        <span>Download</span>
                    </button>
                    <button  className="flex-c-center-center"  onClick={this.downloadCatalogueAsExcel}>
                        <img src={excel} alt='excel' />
                        <span>Export</span>
                    </button>
                  </div>
              </div>
          }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
    isGrtPM: !!state.company.isGrtPM
})

const mapDispatchToProps = dispatch => ({
    showGrtShowroomModal: (catalogueLink,catalogueName) => dispatch(CommonActions.showGrtShowroomModal(catalogueLink,catalogueName)),
    toggleGlobalLoading: isLoading => dispatch(CommonActions.toggleGlobalLoading(isLoading)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ShareCataloguePopup);

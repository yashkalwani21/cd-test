import React from 'react';

import './ContactNumber.css';

const ContactNumber = ({ contact }) => {
  return (
    <div className="inquiryHeader">
      <div className="number">{contact}</div>
    </div>
  );
};

export default ContactNumber;
